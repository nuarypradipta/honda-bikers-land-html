let vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.addEventListener('resize', () => {
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
});

$(document).ready(function(){
  $("#hamburger").click(function(){
    $(this).toggleClass("is-active");
    $('.nav-mobile-overlay').toggleClass("is-open");
  });
});

$(document).ready(function(e) {
  $('img[usemap]').rwdImageMaps();
});

var swiper = new Swiper(".gallery-slider", {
  loop: false,
  speed: 500,
  slidesPerView: 2,
  spaceBetween: 15,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    767: {
      slidesPerView: 3,
    },
    991: {
      slidesPerView: 3,
    }
  }
});
var swiper = new Swiper(".merch-slider", {
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
    renderBullet: function (index, className) {
      return '<span class="' + className + '">' + (index + 1) + "</span>";
    },
  },
});

$(".gallery-popup").click(function(){
  $('#gallery-popup').addClass("open");
  $('body').addClass("disable");
});
$(".button-close").click(function(){
  $('#gallery-popup').removeClass("open");
  $('body').removeClass("disable");
});